clc
close all;
clear all;
global invMap;
goalP =zeros(1,2);
alpha = 0.5;
probabilityThresholdOfEfficiency = 0.8;
heightOfWindow = 20;
widthOfWindow = 20;
coloredMap = imread('circle.png');
grayMap = rgb2gray(coloredMap);
robotMap = imresize(grayMap, 1);
invMap = imcomplement(robotMap);
[numOfRows, numOfColumns] = size(invMap); %size of the map

%%% Start and Goal Point Selecting
figure(1);
imshow(coloredMap);
hold on;
disp('Choose your start Point');
[yStartC, xStartC, but] = ginput(1);
h1 = text(yStartC,xStartC,'START', ...
    'HorizontalAlignment','center', ...
    'Color', [1 0 0], ...
    'FontSize',14);
xStart = round(xStartC);
yStart = round(yStartC);
startP = [xStartC, yStartC];
% Validation test of Start Point
if invMap(xStart,yStart) > 0
    disp('You chose not Valid Start Point, Try again!');
    return;
end
disp('Choose your Goal Point');
[yGoalC, xGoalC, but2] = ginput(1);      % get a point
h2 = text(yGoalC,xGoalC,'GOAL', ...
    'HorizontalAlignment','center', ...
    'Color', [1 0 0], ...
    'FontSize',14);
xGoal = round(xGoalC);
yGoal = round(yGoalC);
goalP = [xGoal, yGoal];

% Validation test of Goal Point
if invMap(xGoal,yGoal) > 0
    disp('You chose unavailable Start Point, Try again!');
    return;
end

chosenMode = 1;

% Manual Mode
if chosenMode == 1
    numOfSolutions = 1;
    Solutions{numOfSolutions} = [];
    %%% Choose points of Path A
    disp('Note: Please Enter the Number Whithout Considering the Start and Goal Points!')
    numOfPathAPoints = input('Enter Number Of Points that Consists the PATH:	');
    for i= 1:numOfPathAPoints
        disp(['Choose the Point ' num2str(i)]);
        [yC, xC, but3] = ginput(1);      % get a point
        h2 = text(yC,xC,'*', ...
            'HorizontalAlignment','center', ...
            'Color', [0 1 0], ...
            'FontSize',14);
        x = round(xC);
        y = round(yC);
        if isempty(Solutions{1})
            Solutions{1} =...
                [startP(1);startP(2)];
        end
        Solutions{1} =...
            cat(2, Solutions{1},[x;y]);
        if i==numOfPathAPoints
            Solutions{1} =...
                cat(2, Solutions{1},[goalP(1);goalP(2)]);
        end
    end
end

Safty=SaftyOfPath(Solutions{1});
Length= LengthOfPath(Solutions{1});
smoothness=smoothnessOfPath(Solutions{1});

disp(['length = ', num2str(Length)]);
disp(['safety = ', num2str(Safty)]);
disp(['smoothness = ', num2str(smoothness)]);