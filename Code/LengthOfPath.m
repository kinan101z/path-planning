function length = LengthOfPath(path)

[row,col]=size(path);
segment_length=zeros(1,col-1);

for i=1:col-1
    segment_length(1,i)=sqrt((path(1,i+1)-path(1,i))^2 + (path(2,i+1)-path(2,i))^2);
end

length=sum(segment_length);