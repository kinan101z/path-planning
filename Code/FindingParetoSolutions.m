function paretoFront=FindingParetoSolutions(objectives)

% According to the article " A Fast Algorithm for Finding the Non Dominated Set
% in Multi objective Optimization", this code has been implemented

% Sort all the solutions in decreasing order of their first objective function
% use the column indices from sort() to sort all rows of fitnessOfSolutions
[~,ind] = sort(objectives(:,1));
sortedList = objectives(ind,:);    % O is the article

% Initialize a set set1 and add first element of sortedList to Set1
set1 = sortedList(1,:);
nondominatedSolutions=false(1,size(objectives,1));
nondominatedSolutions(ind(1))=true;

for i=2:size(sortedList,1)
    
    % Determine the dominated solutions inside set1 by Oi and remove them
    delIndices=[];
    for k = 1:size(set1,1)
        if all(sortedList(i,:)<=set1(k,:)) && any(sortedList(i,:)<set1(k,:))
            % An element of set S1 dominate Oi
            delIndices=[delIndices k];
            nondominatedSolutions(ind(ismember(sortedList,set1(k,:),'rows')))=false;
        end
    end
    set1(delIndices,:)=[];
    
    if isempty(set1)
        set1=sortedList(i,:);
        nondominatedSolutions(ind(i))=true;
        continue;
    end
    
    % Check if the solution Oi is dominated by set1
    nonDominated=true;
    for k = 1:size(set1,1)
        if all(set1(k,:)<=sortedList(i,:)) && any(set1(k,:)<sortedList(i,:))
            % An element of set S1 dominate Oi
            nonDominated=false;
            break;
        end
    end
    
    if nonDominated
        set1=[set1;sortedList(i,:)];
        nondominatedSolutions(ind(i))=true;
    end
end

paretoFront=find(nondominatedSolutions);

end