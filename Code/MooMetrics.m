[row,col]=size(paretoFront);
PF=zeros(col,3);
for i=1:col
    temp=paretoFront(i);
    PF(i,:)=y(temp,:);
end
hv1=HyperVolume(PF(:,1));
hv2=HyperVolume(PF(:,2));
hv3=HyperVolume(PF(:,3));
nomOfDS=numberOfNonDominatedSolutions(PF);
segma=0.01;
ud = UniformDistribution(PF,segma);