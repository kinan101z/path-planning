function [newROI,OPoint]=SectorsProbability(w,wrongPoint)
global xRU;
global yRU;
global xLD;
global yLD;
global invMap;
[row,col]=size(invMap);
newROI=zeros(row,col);

OPoint= [round((xLD+xRU)/2);round((yLD+yRU)/2)];
th = 0:pi/48:2*pi;
r=w-1;
xunit = r * cos(th) + OPoint(1);
yunit = r * sin(th) + OPoint(2);
newPoints=[round(xunit);round(yunit)];
%figure(6);
%plot(xunit,yunit);

ths =-pi/12:pi/6:(23*pi)/12;
rsector=w-1;
xsector = rsector * cos(ths) + OPoint(1);
ysector = rsector * sin(ths) + OPoint(2);
sectorPoints=[round(xsector);round(ysector)];
%figure(7);
%plot(xsector,ysector);
[rows,cols]=size(sectorPoints);
sumOfOccupancies=zeros(1,cols);
for j=1:cols
    SPoint=sectorPoints(:,j)';
    OPoint=OPoint';
    sumOfOccupancies(j) = OccupancyOfSegment(OPoint,SPoint,invMap);
end
maxSumOfOccupancy=max(sumOfOccupancies);
if(maxSumOfOccupancy<=0)
    normalizedOccupancyProbability=zeros(1,cols);
else
    normalizedOccupancyProbability=1-(sumOfOccupancies/maxSumOfOccupancy);
end
%figure(6);
%plot(normalizedOccupancyProbability);
points30=newPoints(:,1:8);
points60=newPoints(:,9:16);
points90=newPoints(:,17:24);
points120=newPoints(:,25:32);
points150=newPoints(:,33:40);
points180=newPoints(:,41:48);
points210=newPoints(:,49:56);
points240=newPoints(:,57:64);
points270=newPoints(:,65:72);
points300=newPoints(:,73:80);
points330=newPoints(:,81:88);
points360=newPoints(:,89:96);

OPoint=OPoint';
[row30,col30]=size(points30);
S30=OPoint;
for i=1:col30
    xs=[OPoint(1),points30(1,i)];
    ys=[OPoint(2),points30(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S30=cat(2,S30,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(2);
    end
end
[row60,col60]=size(points60);
S60=OPoint;
for i=1:col60
    xs=[OPoint(1),points60(1,i)];
    ys=[OPoint(2),points60(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S60=cat(2,S60,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(3);
    end
end
[row90,col90]=size(points90);
S90=OPoint;
for i=1:col90
    xs=[OPoint(1),points90(1,i)];
    ys=[OPoint(2),points90(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S90=cat(2,S90,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(4);
    end
end
[row120,col120]=size(points120);
S120=OPoint;
for i=1:col120
    xs=[OPoint(1),points120(1,i)];
    ys=[OPoint(2),points120(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        if(y1<=0)
            y1=OPoint(2);
        end
        P=[x1;y1];
        S120=cat(2,S120,P);
        newROI(x1,y1)=normalizedOccupancyProbability(5);
    end
end
[row150,col150]=size(points150);
S150=OPoint;
for i=1:col150
    xs=[OPoint(1),points150(1,i)];
    ys=[OPoint(2),points150(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S150=cat(2,S150,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(6);
    end
end
[row180,col180]=size(points180);
S180=OPoint;
for i=1:col180
    xs=[OPoint(1),points180(1,i)];
    ys=[OPoint(2),points180(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S180=cat(2,S180,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(7);
    end
end
[row210,col210]=size(points210);
S210=OPoint;
for i=1:col210
    xs=[OPoint(1),points210(1,i)];
    ys=[OPoint(2),points210(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S210=cat(2,S210,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(8);
    end
end
[row240,col240]=size(points240);
S240=OPoint;
for i=1:col240
    xs=[OPoint(1),points240(1,i)];
    ys=[OPoint(2),points240(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S240=cat(2,S240,P);
        if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(9);
    end
end
[row270,col270]=size(points270);
S270=OPoint;
for i=1:col270
    xs=[OPoint(1),points270(1,i)];
    ys=[OPoint(2),points270(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S270=cat(2,S270,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(10);
    end
end
[row300,col300]=size(points300);
S300=OPoint;
for i=1:col300
    xs=[OPoint(1),points300(1,i)];
    ys=[OPoint(2),points300(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        if(y1<=0)
            y1=OPoint(2);
        end
        P=[x1;y1];
        S300=cat(2,S300,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(11);
    end
end
[row330,col330]=size(points330);
S330=OPoint;
for i=1:col330
    xs=[OPoint(1),points330(1,i)];
    ys=[OPoint(2),points330(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S330=cat(2,S330,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(12);
    end
end
[row360,col360]=size(points360);
S360=OPoint;
for i=1:col360
    xs=[OPoint(1),points360(1,i)];
    ys=[OPoint(2),points360(2,i)];
    startEnd=sort(xs);
    coefficients = polyfit(xs,ys, 1);
    a = coefficients (1);
    b = coefficients (2);
    for x=startEnd(1):startEnd(2)
        y=a*x+b;
        x1=uint32(x);
        y1=uint32(y);
        P=[x1;y1];
        S360=cat(2,S360,P);
                if(y1<=0)
            y1=OPoint(2);
        end
        newROI(x1,y1)=normalizedOccupancyProbability(1);
    end
end

if(wrongPoint(1)~=0 && wrongPoint(2)~=0)
    [row,col]=size(sectorPoints);
    for i=1:col
        d(i)=sqrt((wrongPoint(1)-sectorPoints(1,i))^2+(wrongPoint(2)-sectorPoints(2,i))^2);
    end
    [minimumD minimumDI]=min(d);
    if (minimumDI==1)
        normalizedOccupancyProbability(1)=0;
        [row,col]=size(S360);
        for j=1:col
            x=S360(1,j);
            y=S360(2,j);
            newROI(x,y)=normalizedOccupancyProbability(1);
        end
    elseif (minimumDI==2)
        normalizedOccupancyProbability(2)=0;
        [row,col]=size(S30);
        for j=1:col
            x=S30(1,j);
            y=S30(2,j);
            newROI(x,y)=normalizedOccupancyProbability(2);
        end
    elseif (minimumDI==3)
        normalizedOccupancyProbability(3)=0;
        [row,col]=size(S60);
        for j=1:col
            x=S60(1,j);
            y=S60(2,j);
            newROI(x,y)=normalizedOccupancyProbability(3);
        end
    elseif (minimumDI==4)
        normalizedOccupancyProbability(4)=0;
        [row,col]=size(S90);
        for j=1:col
            x=S90(1,j);
            y=S90(2,j);
            newROI(x,y)=normalizedOccupancyProbability(4);
        end
    elseif (minimumDI==5)
        normalizedOccupancyProbability(5)=0;
        [row,col]=size(S120);
        for j=1:col
            x=S120(1,j);
            y=S120(2,j);
            newROI(x,y)=normalizedOccupancyProbability(5);
        end
    elseif (minimumDI==6)
        normalizedOccupancyProbability(6)=0;
        [row,col]=size(S150);
        for j=1:col
            x=S150(1,j);
            y=S150(2,j);
            newROI(x,y)=normalizedOccupancyProbability(6);
        end
    elseif (minimumDI==7)
        normalizedOccupancyProbability(7)=0;
        [row,col]=size(S180);
        for j=1:col
            x=S180(1,j);
            y=S180(2,j);
            newROI(x,y)=normalizedOccupancyProbability(7);
        end
    elseif (minimumDI==8)
        normalizedOccupancyProbability(8)=0;
        [row,col]=size(S210);
        for j=1:col
            x=S210(1,j);
            y=S210(2,j);
            newROI(x,y)=normalizedOccupancyProbability(8);
        end
    elseif (minimumDI==9)
        normalizedOccupancyProbability(9)=0;
        [row,col]=size(S240);
        for j=1:col
            x=S240(1,j);
            y=S240(2,j);
            newROI(x,y)=normalizedOccupancyProbability(9);
        end
    elseif (minimumDI==10)
        normalizedOccupancyProbability(10)=0;
        [row,col]=size(S270);
        for j=1:col
            x=S270(1,j);
            y=S270(2,j);
            newROI(x,y)=normalizedOccupancyProbability(10);
        end
    elseif (minimumDI==11)
        normalizedOccupancyProbability(11)=0;
        [row,col]=size(S300);
        for j=1:col
            x=S300(1,j);
            y=S300(2,j);
            newROI(x,y)=normalizedOccupancyProbability(11);
        end
    elseif (minimumDI==12)
        normalizedOccupancyProbability(12)=0;
        [row,col]=size(S330);
        for j=1:col
            x=S330(1,j);
            y=S330(2,j);
            newROI(x,y)=normalizedOccupancyProbability(12);
        end
    end
end

x=OPoint(1);
y=OPoint(2);
newROI(x,y)=0;
newROI(x+1,y)=0;
newROI(x,y+1)=0;
newROI(x-1,y)=0;
newROI(x,y-1)=0;
newROI(x+1,y-1)=0;
newROI(x-1,y-1)=0;
newROI(x+1,y+1)=0;
newROI(x-1,y-1)=0;

end