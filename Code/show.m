imshow(grayMap);
hold on
color = rand(3);
[row,col]=size(paretoFront);

for h=1:col
    bestSolution=solutions{paretoFront(h)};
    bestSolution=round(bestSolution);
    [rowi,coli]=size(bestSolution);
    text(bestSolution(2,:),bestSolution(1,:),'*', ...
        'HorizontalAlignment','center', ...
        'Color', [color(1) color(2) color(3)], ...
        'FontSize',14);
end
hold off