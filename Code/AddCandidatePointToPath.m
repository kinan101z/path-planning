function [newStartPoint, previousPoint, isGoalArrived, count,probabilisticMap] =...
    AddCandidatePointToPath(robotMap, probabilisticMap, currentStartPoint,...
    previousPoint, startPoint, goalPoint,...
    probabilityThresholdOfEfficiencyInit,count)
probabilityThresholdOfEfficiency = probabilityThresholdOfEfficiencyInit;
heightOfWindow = 22;
widthOfWindow = 22;
global xRD;
global yRD;
global xLD;
global yLD;
wrongPointCounter = 1;
xCorners = ones(1,5);
yCorners = ones(1,5);
[numOfRows, numOfColumns] = size(robotMap);
probabilityDensityIdx =  [];
[window, slopeAngle, xCorners, yCorners, ~] =...
    ConstructWindow(robotMap, currentStartPoint, startPoint, goalPoint,...
    xCorners, yCorners, heightOfWindow, widthOfWindow);% Construct the first window

figure(5);
imshow(window);
%drawnow
%hold on;

global safe;

% apply the window on the probabilistic map
regionOfInterest =  probabilisticMap .* window;
[newMap,Opoint]=SectorsProbability(widthOfWindow,previousPoint);
[rowr,colr]=size(regionOfInterest);
[rown,coln]=size(newMap);
if(rowr==rown && colr==coln)
    regionOfInterest=(regionOfInterest+newMap)/2;
end
imshow(regionOfInterest);
isGoalArrived = regionOfInterest(goalPoint(1),goalPoint(2));
effeciencyOfROI = sum(sum(regionOfInterest));
%numOfWindowCells = sum(sum(window));
%expandingHeightCounter = 1;

%epsilon = pi;
safeFlag=false;
while (safeFlag==false)
    
    % Construct the Probability Density (PD)
    if isGoalArrived>0
        newStartPointIdx = sub2ind([numOfRows, numOfColumns], goalPoint);
        currentStartPoint = regionOfInterest(goalPoint(1),goalPoint(2));
        newStartPoint = newStartPointIdx;
        safeFlag = true;
    else

        normalizedProbability = regionOfInterest ./ effeciencyOfROI;
        
        minNormalizedProbability =...
            min(normalizedProbability(normalizedProbability>0));
        
        [sortedProbs, probsIdx] = sort(normalizedProbability(:),'descend');
        
        probabilityDensityRepetition =...
            sortedProbs / minNormalizedProbability;
        zeroIdx = find(sortedProbs==0,1);
        
        
        for i=1:zeroIdx-1
            probabilityDensityIdx = cat(1, probabilityDensityIdx,...
                (repmat(probsIdx(i), 1, round(probabilityDensityRepetition(i))))');
        end
        Idx = find(randi(length(probabilityDensityIdx))<...
            cumsum(1:length(probabilityDensityIdx)),1);
        
        newStartPointIdx = probabilityDensityIdx(Idx);
        [newStartPointTemp(1),newStartPointTemp(2)] =...
            ind2sub([numOfRows, numOfColumns],...
            newStartPointIdx);
        
        wrongPointCounter = wrongPointCounter + 1;
        
        safeSeg=SafetyOfSegment(currentStartPoint,newStartPointTemp,robotMap);
        occupancy=robotMap(newStartPointTemp(1),newStartPointTemp(2));
        
        if occupancy>=0
            [newMap,OPoint]=SectorsProbability(widthOfWindow,newStartPointTemp);
            regionOfInterest=(regionOfInterest+newMap)/2;
            imshow(regionOfInterest);
            effeciencyOfROI = sum(sum(regionOfInterest));
        end
        
        if (safeSeg <= 20 && occupancy==0)
            safeFlag = true;
            newStartPoint = newStartPointTemp;
            wrongPointCounter = 1;
        end
        
        if wrongPointCounter>=17
            if(goalPoint(2)>startPoint(2))
                if(goalPoint(1)>startPoint(1))
                    newStartPoint(1) = round(xRD);
                    newStartPoint(2)= round(yRD);
                else
                    newStartPoint(1) = round(xLD);
                    newStartPoint(2)= round(yLD);
                end
            else
                if(goalPoint(1)>startPoint(1))
                    newStartPoint(1) = round(xLD);
                    newStartPoint(2)= round(yLD);
                else
                    newStartPoint(1) = round(xRD);
                    newStartPoint(2)= round(yRD);
                end
            end
            safeFlag = true;
            wrongPointCounter = 1;
        end
    end
end
previousPoint = currentStartPoint;
end