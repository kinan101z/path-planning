clc
close all;
clear all;

%%% Initializaion
%global goal;
bestSolution=[];
numOfGenerations=1;
probabilityMovement = 0.05;
global segmentSafty;
global safe;
goal = zeros(1,2);
alpha = 0.5;
probabilityThresholdOfEfficiency = 0.8;
numOfSolutions = 2;
coloredMap = imread('poligon density.png');
figure(1);
imshow(coloredMap);
grayMap = rgb2gray(coloredMap);
robotMap = imresize(grayMap, 1);
global invMap;
invMap = imcomplement(robotMap);
[numOfRows, numOfColumns] = size(invMap); %size of the map
figure(2);
imshow(grayMap);
hold on;
disp('Choose your start Point');
[yStartC, xStartC, but] = ginput(1);
h1 = text(yStartC,xStartC,'START', ...
    'HorizontalAlignment','center', ...
    'Color', [1 0 0], ...
    'FontSize',14);
xStart = round(xStartC);
yStart = round(yStartC);
start = [xStartC, yStartC];

% Validation test of Start Point
if invMap(xStart,yStart) > 0
    disp('You chose not Valid Start Point, Try again!');
    return;
end
disp('Choose your Goal Point');
[yGoalC, xGoalC, but2] = ginput(1);      % get a point
h2 = text(yGoalC,xGoalC,'GOAL', ...
    'HorizontalAlignment','center', ...
    'Color', [1 0 0], ...
    'FontSize',14);
xGoal = round(xGoalC);
yGoal = round(yGoalC);
goal = [xGoalC, yGoalC];
hold off;
% Validation test of Goal Point
if invMap(xGoal,yGoal) > 0
    disp('You chose unavailable Start Point, Try again!');
    return;
end

%%% Find Probabilities
maxOccupation = max(invMap(:));
[distance,slope] = DistanceBetweenEachPointAndLineSegement([numOfRows, numOfColumns],start, goal);
maxDist = max(distance(:));
probabilitiesMap = alpha * (repmat(double(maxDist), numOfRows, numOfColumns) -...
    distance) ./ repmat(double(maxDist), numOfRows, numOfColumns)...
    + (1-alpha)* (repmat(double(maxOccupation), numOfRows, numOfColumns)-...
    double(invMap)) ./ repmat(double(maxOccupation), numOfRows, numOfColumns);
%hold off;

figure(3);
imshow(probabilitiesMap);

%%% Initilization of solutions
solutions = GenerateSolutions(numOfSolutions,...
    invMap, probabilitiesMap,  start, goal,...
    probabilityThresholdOfEfficiency);
[y,length,safety,smoothness]=PpObjectives(solutions);
paretoFront=FindingParetoSolutions(y);

for k=1:numOfGenerations
    [row,col]=size(paretoFront);
    for h=1:col
        bestSolution=solutions{paretoFront(h)};
        bestSolution=round(bestSolution);
        probabilitiesMap=MovingToBest(bestSolution,probabilityMovement,probabilitiesMap,invMap);
    end
    
    delta=rand;
    if(rand<0.1)
        probabilitiesMap = alpha * (repmat(double(maxDist), numOfRows, numOfColumns) -...
            distance) ./ repmat(double(maxDist), numOfRows, numOfColumns)...
            + (1-alpha)* (repmat(double(maxOccupation), numOfRows, numOfColumns)-...
            double(invMap)) ./ repmat(double(maxOccupation), numOfRows, numOfColumns);
    end
    solutions = GenerateSolutions(numOfSolutions,...
        invMap, probabilitiesMap,  start, goal,...
        probabilityThresholdOfEfficiency);
    [y,length,safety,smoothness]=PpObjectives(solutions);
    paretoFront=FindingParetoSolutions(y);
end

%paretoFront=FindingParetoSolutions(y);
imshow(coloredMap);
hold on
[row,col]=size(paretoFront);
for h=1:col
    bestSolution=solutions{paretoFront(h)};
    bestSolution=round(bestSolution);
    [rowi,coli]=size(bestSolution);
    color = rand(3);
    text(bestSolution(2,:),bestSolution(1,:),'*', ...
        'HorizontalAlignment','center', ...
        'Color', [color(1) color(2) color(3)], ...
        'FontSize',14);
    
    f = fit(bestSolution(2,:)',bestSolution(1,:)','smoothingspline','SmoothingParam',0.1);
    plot(f,bestSolution(2,:)',bestSolution(1,:)')
end
hold off
save('Vnew');