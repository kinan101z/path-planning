function sumOfOccupancy=SafetyOfSegment(previousPoint,newStart,robotMap)
count = 0;
global safe;
safe=0;
sumOfOccupancy=0;

[row1,col1]=size(previousPoint);
[row2,col2]=size(newStart);

if(col1==2 && col2==2)
    xs=[previousPoint(1),newStart(1)];
    ys=[previousPoint(2),newStart(2)];
    startEnd=sort(xs);
    d=sqrt((previousPoint(1)-newStart(1))^2+(previousPoint(2)-newStart(2))^2);
    if(d>5)
        coefficients = polyfit(xs,ys, 1);
        a = coefficients (1);
        b = coefficients (2);
        
        for x=startEnd(1):startEnd(2)
            y=a*x+b;
            x1=uint32(x);
            y1=uint32(y);
            
            if(x1<=0)
                x1=round((-1*x1)+1);
            end
            if(y1<=0)
                y1=round((-1*y1)+1);
            end
            [row,col]=size(robotMap);
            if(x1>=row)
                x1=row-1;
            end
            if(y1>=col)
                y1=col-1;
            end
            occupancy=robotMap(x1,y1);
            if occupancy>0
                count=count+1;
                sumOfOccupancy=sumOfOccupancy+double(occupancy);
            end
        end
        if(count>5)
            safe=1;
        end
    end
end
end