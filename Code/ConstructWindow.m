function [window, slopeAngle, xCorners, yCorners, borderKeeperFlag] =...
    ConstructWindow(robotMap, startPoint,initialStart, goalPoint,...
    xCorners, yCorners, heightOfWindow, widthOfWindow)

global xRD;
global xRU;
global yRD;
global yRU;
global xLD;
global yLD;
global xLU;
global yLU;


%slopeAngle = atan2(goalPoint(2) - initialStart(2), goalPoint(1) - initialStart(1));
slopeAngle = atan2(goalPoint(2) - startPoint(2), goalPoint(1) - startPoint(1));


%% find X & Y coordinates of the window corners

% xRD, yRD: x & y of the Right-Down corner respectively
% xLU, yLU: x & y of the Left-Upper corner respectively
% and so on...
xRD = startPoint(1) + heightOfWindow * cos(-pi/2 + slopeAngle);
yRD = startPoint(2) + heightOfWindow * sin(-pi/2 + slopeAngle); % DON'T FORGET TO CHECK THIS RULE
xLD = startPoint(1) + heightOfWindow * cos(pi/2 + slopeAngle);
yLD = startPoint(2) + heightOfWindow * sin(pi/2 + slopeAngle);% DON'T FORGET TO CHECK THIS RULE
xRU = xRD + 2*widthOfWindow*cos(slopeAngle);
yRU = yRD + 2*widthOfWindow*sin(slopeAngle);
xLU = xLD + 2*widthOfWindow*cos(slopeAngle);
yLU = yLD + 2*widthOfWindow*sin(slopeAngle);

% Construction of the window
xCornersTmp = [xRD, xRU, xLU, xLD,  xRD];
yCornersTmp = [yRD, yRU, yLU, yLD, yRD];
if all(xCornersTmp>=0) && all(yCornersTmp>=0)&& ~all(xCornersTmp == 0) && ~all(yCornersTmp == 0)
    xCorners = xCornersTmp;
    yCorners = yCornersTmp;
    borderKeeperFlag = false;
else
    borderKeeperFlag = true;
end
[numOfRows, numOfColumns] = size(robotMap);
window = poly2mask(yCorners, xCorners, numOfRows, numOfColumns);

end