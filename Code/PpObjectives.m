function [y, length,safety, smoothness] = PpObjectives(solutions)

[row , col]=size(solutions);
length = zeros(1,col);
PointSafety = zeros(1,col);
smoothness = zeros(1,col);
global segmentSafty

for i=1:col 
    length(1,i) = LengthOfPath(solutions{i});
    
    PointSafety(1,i) = SaftyOfPath(solutions{i});
    
    [smoothness(1,i),angles] = smoothnessOfPath(solutions{i});
end
safety=PointSafety+segmentSafty;
y(:,1)=length;
y(:,2)=safety;
y(:,3)=smoothness;
end