function occupancy = SaftyOfPath(path)

global invMap;

[row,col]= size(path);

points_occupancy = zeros(1,col-1);
x=round(path(1,:));
y=round(path(2,:));

for i=1:col-1    
    points_occupancy(1,i)=invMap(x(i+1),y(i+1));
end

occupancy = sum(points_occupancy);