function probabilitiesMap=MovingToBest(bestSolution,probabilityMovement,PM,invMap)
probabilitiesMap=PM;
[rowi,coli]=size(bestSolution);
for i=1:coli
    if(invMap(bestSolution(1,i),bestSolution(2,i)==0))
        probabilitiesMap(bestSolution(1,i),bestSolution(2,i))=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement;
        probabilitiesMap(bestSolution(1,i)+1,bestSolution(2,i))=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement/2;
        probabilitiesMap(bestSolution(1,i)-1,bestSolution(2,i))=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement/2;
        probabilitiesMap(bestSolution(1,i),bestSolution(2,i)+1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement/2;
        probabilitiesMap(bestSolution(1,i),bestSolution(2,i)-1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement/2;
        probabilitiesMap(bestSolution(1,i)+1,bestSolution(2,i)+1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement/4;
        probabilitiesMap(bestSolution(1,i)-1,bestSolution(2,i)-1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement/4;
        probabilitiesMap(bestSolution(1,i)+1,bestSolution(2,i)-1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement/4;
        probabilitiesMap(bestSolution(1,i)-1,bestSolution(2,i)+1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))+probabilityMovement/4;
    else
        probabilitiesMap(bestSolution(1,i),bestSolution(2,i))=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement;
        probabilitiesMap(bestSolution(1,i)+1,bestSolution(2,i))=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement/2;
        probabilitiesMap(bestSolution(1,i)-1,bestSolution(2,i))=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement/2;
        probabilitiesMap(bestSolution(1,i),bestSolution(2,i)+1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement/2;
        probabilitiesMap(bestSolution(1,i),bestSolution(2,i)-1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement/2;
        probabilitiesMap(bestSolution(1,i)+1,bestSolution(2,i)+1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement/4;
        probabilitiesMap(bestSolution(1,i)-1,bestSolution(2,i)-1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement/4;
        probabilitiesMap(bestSolution(1,i)+1,bestSolution(2,i)-1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement/4;
        probabilitiesMap(bestSolution(1,i)-1,bestSolution(2,i)+1)=probabilitiesMap(bestSolution(1,i),bestSolution(2,i))-probabilityMovement/4;
    end
end
end