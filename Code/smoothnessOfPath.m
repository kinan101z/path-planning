function [anglesSum,angles] = smoothnessOfPath(path)

[row,col]= size(path);

angles=zeros(1,col-2);

for i=1:col-2

    v1=[path(1,i+1),path(2,i+1),0]-[path(1,i),path(2,i),0];
    v2=[path(1,i+2),path(2,i+2),0]-[path(1,i+1),path(2,i+1),0];
    
    angles(1,i)= mod(atan2d(norm(cross(v1,v2)),dot(v1,v2)),360);
end

anglesSum = sum(angles);
