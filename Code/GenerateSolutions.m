function Solutions = GenerateSolutions(numOfSolutions,...
    robotMap, probabilisticMap, startPoint, goalPoint,...
    occupancyThresholdOfROI)

counterOfSolutionsNumber = 1;
%[numOfRows, numOfColumns] = size(map);
Solutions{numOfSolutions} = [];
isGoalArrived =0;
rngSeed = 1;

figure(4);
imshow(robotMap);
global segmentSafty;
global safe;
segmentSafty=zeros(1,numOfSolutions);
while counterOfSolutionsNumber <= numOfSolutions
    newStart = startPoint;
    previousPoint = startPoint;
    rng(rngSeed);
    color = rand(3);
    while isGoalArrived==0
        
        [newStart, previousPoint, isGoalArrived, counterOfSolutionsNumber,probabilisticMap]...
            = AddCandidatePointToPath(robotMap, probabilisticMap, newStart,...
            previousPoint, startPoint, round(goalPoint),...
            occupancyThresholdOfROI, counterOfSolutionsNumber);
        
        sumOfOccupancy=SafetyOfSegment(previousPoint,newStart,robotMap);
        segmentSafty(1,counterOfSolutionsNumber)=segmentSafty(1,counterOfSolutionsNumber)+sumOfOccupancy;
        if(safe==1)
            disp('danger');
        end
        
        if isempty(Solutions{counterOfSolutionsNumber})
            Solutions{counterOfSolutionsNumber} =...
                [startPoint(1);startPoint(2)];
            Solutions{counterOfSolutionsNumber} =...
                cat(2, Solutions{counterOfSolutionsNumber},(newStart)');
            
            h3 = text(newStart(2),newStart(1),'*', ...
                'HorizontalAlignment','center', ...
                'Color', [color(1) color(2) color(3)], ...
                'FontSize',14);
            drawnow;
        else
            Solutions{counterOfSolutionsNumber} =...
                cat(2, Solutions{counterOfSolutionsNumber},(newStart)');
            h4 = text(newStart(2),newStart(1),'*', ...
                'HorizontalAlignment','center', ...
                'Color', [color(1) color(2) color(3)], ...
                'FontSize',14);
            drawnow;
        end
    end
    if isGoalArrived > 0
        if isempty(Solutions{counterOfSolutionsNumber})
            Solutions{counterOfSolutionsNumber} =...
                [startPoint(1);startPoint(2)];
            Solutions{counterOfSolutionsNumber} =...
                cat(2, Solutions{counterOfSolutionsNumber},(newStart)');
            h5 = text(newStart(2),newStart(1),'*', ...
                'HorizontalAlignment','center', ...
                'Color', [color(1) color(2) color(3)], ...
                'FontSize',14);
            drawnow;
        else
            Solutions{counterOfSolutionsNumber} =...
                cat(2, Solutions{counterOfSolutionsNumber},(newStart)');
            h6 = text(newStart(2),newStart(1),'*', ...
                'HorizontalAlignment','center', ...
                'Color', [color(1) color(2) color(3)], ...
                'FontSize',14);
            drawnow;
        end
        counterOfSolutionsNumber = counterOfSolutionsNumber +1;
        rngSeed = rngSeed +1;
        isGoalArrived =0;
    end
end
end